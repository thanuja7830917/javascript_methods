function mapObject(obj,cb){
    const result=[]
    if(Object.keys(obj).length>0){
        for (let key in obj){
            result.push(cb(key,obj))
        }
        return result
    }
    else{
        return "input is empty object"
    }

}
function cb(key,obj){
    return `"${key}" :${obj[key]}`
}

module.exports={mapObject,cb}